# README #

* Library

A simple PHP library that will calculate the mean, median, mode, and range of a set of numbers.  Methods should accept an array of numbers.  All returned values are rounded to a maximum of 3 decimal places. If a return value does not exist, NULL is returned.

* API

API implements a single endpoint "/mmmr" with no query string parameters. When a POST request is called to "/mmmr", a single JSON object will be passed with an attribute called "numbers".  "numbers" is a JSON array of n numbers that should be processed. The mean, median, mode and range of the numbers are returned as a JSON object. If a return value does not exist, an empty string is returned. All system errors return a JSON error response and a 500 error. If a request is sent to "/mmmr" that is not a POST request, a JSON response and a 404 error is returned.

### How do I get set up? ###

* Installation


```
#!bash

git clone https://adamheinz@bitbucket.org/adamheinz/mmmr.git
cd mmmr
vagrant up

```

* How to run tests

```
#!bash

cd /vagrant/app
./vendor/bin/phpunit lib/
```
