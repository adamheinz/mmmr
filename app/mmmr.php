<?php
use Symfony\Component\HttpFoundation\Request;

require_once('vendor/autoload.php');

$request = Request::createFromGlobals();
$controller = new \MMMR\MathController();
$controller->mmmrAction($request);
