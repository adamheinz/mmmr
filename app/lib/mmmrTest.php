<?php
namespace MMMR;

use Symfony\Component\HttpFoundation\Response;

class mmmrTest extends \PHPUnit_Framework_TestCase
{
	private $curl;

	public function setUp() {
		$this->curl = curl_init();
	}

	public function tearDown() {
		curl_close($this->curl);
	}

	/**
	 * @dataProvider \MMMR\MathTestDataProvider::dataProvider
	 */
	public function test_post_mmmr($expects, $values) {
		curl_setopt($this->curl, CURLOPT_POST, true);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, array('numbers' => json_encode($values)));
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_URL, 'http://localhost/mmmr');
		$response = curl_exec($this->curl);
		array_walk($expects, function(&$value) {
			if (null === $value) {
				$value = "";
			}
		});
		$this->assertSame(array('results' => $expects), json_decode($response, true));

		$code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
		$this->assertEquals(Response::HTTP_OK, $code);
	}

	public function test_get_mmmr() {
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_URL, 'http://localhost/mmmr?numbers=' . json_encode(array(1,2,3)));
		$response = json_decode(curl_exec($this->curl), true);
		$this->assertEquals(Response::HTTP_NOT_FOUND, $response['code']);
		$this->assertArrayHasKey('message', $response);

		$code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
		$this->assertEquals(Response::HTTP_NOT_FOUND, $code);
	}
}
