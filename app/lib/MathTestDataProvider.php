<?php
namespace MMMR;

class MathTestDataProvider
{
	public static function dataProvider() {
		return array(
			array(
				array(
					"mean" => 15,
					"median" => 14,
					"mode" => 13,
					"range" => 8,
				),
				array(13, 18, 13, 14, 13, 16, 14, 21, 13),
			),
			array(
				array(
					"mean" => 3.5,
					"median" => 3,
					"mode" => null,
					"range" => 6,
				),
				array(1, 2, 4, 7),
			),
			array(
				array(
					"mean" => 10.5,
					"median" => 10.5,
					"mode" => array(10, 11),
					"range" => 5,
				),
				array(8, 9, 10, 10, 10, 11, 11, 11, 12, 13),
			),
			array(
				array(
					"mean" => null,
					"median" => null,
					"mode" => null,
					"range" => null,
				),
				array(1, "bogus", 3),
			),
			array(
				array(
					"mean" => null,
					"median" => null,
					"mode" => null,
					"range" => null,
				),
				array(),
			),
			array(
				array(
					"mean" => 74948116.750,
					"median" => 3.142,
					"mode" => 3.142,
					"range" => 299792455.282,
				),
				array(2.71828, 3.14159, 3.14159, 299792458),
			),
			array(
				array(
					"mean" => 42,
					"median" => 42,
					"mode" => null,
					"range" => 0,
				),
				array(42),
			),
		);
	}
}
