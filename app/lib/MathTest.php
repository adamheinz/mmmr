<?php
namespace MMMR;

class MathTest extends \PHPUnit_Framework_TestCase
{
	private $math;

	public function setUp() {
		$this->math = new Math();
	}

	/**
	 * @dataProvider \MMMR\MathTestDataProvider::dataProvider
	 */
	public function test_mean($expects, $values) {
		$this->callTest($expects, $values, "mean");
	}

	/**
	 * @dataProvider \MMMR\MathTestDataProvider::dataProvider
	 */
	public function test_median($expects, $values) {
		$this->callTest($expects, $values, "median");
	}

	/**
	 * @dataProvider \MMMR\MathTestDataProvider::dataProvider
	 */
	public function test_mode($expects, $values) {
		$this->callTest($expects, $values, "mode");
	}

	/**
	 * @dataProvider \MMMR\MathTestDataProvider::dataProvider
	 */
	public function test_range($expects, $values) {
		$this->callTest($expects, $values, "range");
	}

	protected function callTest($expects, $values, $method) {
		$actual = $this->math->$method($values);
		$this->assertEquals($expects[$method], $actual);
	}
}
