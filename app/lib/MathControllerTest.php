<?php
namespace MMMR;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MathControllerTest extends \PHPUnit_Framework_TestCase
{
	private $controller;

	public function setUp() {
		$this->controller = $this->getMock('\MMMR\MathController', array('output'));
	}

	/**
	 * @dataProvider \MMMR\MathTestDataProvider::dataProvider
	 */
	public function test_mmmrAction($expects, $values) {
		$request = Request::create("mmmr", 'POST', array('numbers' => json_encode($values)));
		$this->controller->expects($this->once())->method('output')->with($this->equalTo(array('results' => $expects)), $this->equalTo(Response::HTTP_OK));
		$this->controller->mmmrAction($request);
	}
}
