<?php
namespace MMMR;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @license MIT
 */
class MathController
{
	private $response;

	public function mmmrAction(Request $request) {
		if (!$request->request->all()) {
			return $this->error("Request must be POST", Response::HTTP_NOT_FOUND);
		}
		$numbers = json_decode($request->request->get('numbers'));
		if (!is_array($numbers)) {
			return $this->error("Request must contain 'numbers' array");
		}
		$math = new Math();
		$response = array(
			'mean' => $math->mean($numbers),
			'median' => $math->median($numbers),
			'mode' => $math->mode($numbers),
			'range' => $math->range($numbers),
		);
		array_walk($response, function(&$value) {
			if (null === $value) {
				$value = "";
			}
		});
		$this->output(array('results' => $response));
	}

	private function error($message, $code = Response::HTTP_INTERNAL_SERVER_ERROR) {
		$response = array('code' => $code, 'message' => $message);
		$this->output($response, $code);
	}

	protected function output($content, $code = Response::HTTP_OK) {
		$response = new Response(json_encode($content), $code, array('Content-Type', 'text/javascript'));
		$response->send();
	}
}
