<?php
namespace MMMR;

/**
 * @license MIT
 */
class Math
{
	/**
	 * Calculate mean for an array of numbers.
	 * If empty array, returns null.
	 * Otherwise returns mean.
	 * @param $argv number[]
	 * @return mixed
	 */
	public function mean($argv) {
		$argc = count($argv);
		if (!$this->valid($argv, $argc)) {
			return null;
		}
		$sum = array_sum($argv);
		$mean = $sum / $argc;
		return $this->round($mean);
	}

	/**
	 * Calculate median for an array of numbers.
	 * If empty array, returns null.
	 * If array count is even, returns mean of median two numbers.
	 * Otherwise returns median number.
	 * @param $argv number[]
	 * @return mixed
	 */
	public function median($argv) {
		$argc = count($argv);
		if (!$this->valid($argv, $argc)) {
			return null;
		}
		sort($argv);
		if ($argc % 2) {
			$index = ($argc - 1) / 2;
			$median = $this->round($argv[$index]);
		} else {
			$index = ($argc / 2) - 1;
			$median = $this->mean(array($argv[$index], $argv[$index + 1]));
		}
		return $median;
	}

	/**
	 * Calculate mode for an array of numbers.
	 * If empty array or no number occurs more than once, returns null.
	 * If multiple numbers tie for most frequent, returns mode array, sorted ascending.
	 * Otherwise returns mode.
	 * @param $argv number[]
	 * @return mixed
	 */
	public function mode($argv) {
		$argc = count($argv);
		if (!$this->valid($argv, $argc)) {
			return null;
		}
		$counts = array_count_values(array_map("strval", $argv));
		$max = max($counts);
		if ($max > 1) {
			$modes = array_filter($counts, function($val) use ($max) {
				return ($val == $max);
			});
			if (count($modes) > 1) {
				$mode = array_map(array($this, "round"), array_keys($modes));
				sort($mode);
			} else {
				$mode = $this->round(key($modes));
			}
		} else {
			$mode = null;
		}
		return $mode;
	}

	/**
	 * Calculate range for an array of numbers.
	 * If empty array, returns null.
	 * Otherwise returns maximum minus minimum.
	 * @param $argv number[]
	 * @return mixed
	 */
	public function range($argv) {
		$argc = count($argv);
		if (!$this->valid($argv, $argc)) {
			return null;
		}
		sort($argv);
		$range = $argv[$argc - 1] - $argv[0];
		return $this->round($range);
	}

	private function round($value) {
		return round($value, 3);
	}

	private function valid($argv, $argc) {
		if (!$argc) {
			return false;
		}
		if (count(array_filter($argv, "is_numeric")) != $argc) {
			return false;
		}
		return true;
	}
}
