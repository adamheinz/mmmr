node default {
  class { '::apache':
  	mpm_module => "prefork",
  }

  class { '::apache::mod::php': }

  package { "php-xml": }

  class { 'firewall': }

  firewall { '100 allow http access':
    port => 80,
    proto => tcp,
    action => accept,
  }
}
